package com.example.tokomajujaya;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
public class MhsAdapter extends RecyclerView.Adapter<MhsAdapter.MhsVH> {
    private ArrayList<MhsModel> mhslist;
    private final OnItemClickListener listener;
    public MhsAdapter(ArrayList<MhsModel> mhslist, OnItemClickListener
            listener) {
        this.mhslist = mhslist;
        this.listener = listener;
    }
    @NonNull
    @Override
    public MhsAdapter.MhsVH onCreateViewHolder(@NonNull ViewGroup parent, int
            viewType) {
        LayoutInflater LayoutInflater =
                android.view.LayoutInflater.from(parent.getContext());
        View v = LayoutInflater.inflate(R.layout.item_listmhs, parent,
                false);
        return new MhsVH(v);
    }
    @Override
    public void onBindViewHolder(@NonNull MhsVH holder, int position) {
        holder.tvNamaVal.setText(mhslist.get(position).getNama());
        holder.tvNimVal.setText(mhslist.get(position).getNim());
        holder.tvNoHpVal.setText(mhslist.get(position).getNoHp());
        holder.bind(mhslist, position, listener);
    }
    public interface OnItemClickListener{
        void OnItemClick(ArrayList<MhsModel> mhsList, int position);
    }
    public void removeItem(int position){
        this.mhslist.remove(position);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return mhslist.size();
    }
    public class MhsVH extends RecyclerView.ViewHolder{
        private TextView tvNamaVal, tvNimVal, tvNoHpVal;
        private CardView cvItem;
        public MhsVH(@NonNull View itemView) {
            super(itemView);
            tvNamaVal = itemView.findViewById(R.id.tvNamaVal);
            tvNimVal = itemView.findViewById(R.id.tvNimVal);
            tvNoHpVal = itemView.findViewById(R.id.tvNoHpVal);
            cvItem = itemView.findViewById(R.id.cvItem);
        }
        public void bind(ArrayList<MhsModel> mhslist, int position,
                         OnItemClickListener listener){
            cvItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnItemClick(mhslist, position);
                    notifyDataSetChanged();
                }
            });
        }
    }
}
