package com.example.tokomajujaya;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String username_Valid = "maju";
        String password_valid = "jaya";
        EditText edUsername = (EditText) findViewById(R.id.edUsername);
        EditText edPassword = (EditText) findViewById(R.id.edPassword);
        Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            //Komentar
            @Override
            public void onClick(View view) {
                if( (edUsername.getText().toString().trim().isEmpty())
                        || (edPassword.getText().toString().trim().isEmpty()) ) {
                    // JIKA KEDUA ATAU SALAH SATU ISIAN ADA YANG KOSONG
                    Toast.makeText(getApplicationContext(), "Isian tidak valid", Toast.LENGTH_SHORT).show();
                }else{
                    // JIKA KEDUA ISIAN TERISI
                }
                if( (edUsername.getText().toString().trim().equals
                        (username_Valid)) && (edPassword.getText().toString().trim().equals
                        (password_valid)) ){
                    // Toast.makeText(getApplicationContext(), "Login berhasil", Toast.LENGTH_SHORT).show();
                    Intent intent_home = new Intent(MainActivity.this, Home.class);
                    intent_home.putExtra("username", username_Valid);
                    startActivity(intent_home);
                }else{
                    Toast.makeText(getApplicationContext(), "Login gagal", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
