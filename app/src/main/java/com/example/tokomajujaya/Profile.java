package com.example.tokomajujaya;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Button btnKembaliP = findViewById(R.id.btnKembaliP);
        ImageView iv1 = findViewById(R.id.iv1);
            iv1.setImageResource(R.drawable.proyecto_mobiliario_librer_a_papeler_a_ofinq_en_sevilla);


        btnKembaliP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btnKembaliP = new Intent(Profile.this, Home.class);
                startActivity(btnKembaliP);
            }
        });
    }
}