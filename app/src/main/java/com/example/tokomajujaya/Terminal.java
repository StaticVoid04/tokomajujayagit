package com.example.tokomajujaya;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.ArrayList;
public class Terminal extends AppCompatActivity {
    ArrayList<MhsModel> mhsList;
    MhsModel mm;
    DbHelper db;
    boolean isEdit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminal);
        EditText edNama = (EditText) findViewById(R.id.edNama);
        EditText edNim = (EditText) findViewById(R.id.edNim);
        EditText edNoHp = (EditText) findViewById(R.id.edNoHp);
        Button btnSimpan = (Button) findViewById(R.id.btnSimpan);
        Button btnKembali = (Button) findViewById(R.id.btnKembali);
        mhsList = new ArrayList<>();
        isEdit = false;
        Intent intent_main = getIntent();
        if(intent_main.hasExtra("mhsData")){
            mm = intent_main.getExtras().getParcelable("mhsData");
            edNama.setText(mm.getNama());
            edNim.setText(mm.getNim());
            edNoHp.setText(mm.getNoHp());
            isEdit = true;
            btnSimpan.setBackgroundColor(Color.BLUE);
            btnSimpan.setText("Edit");
        }
        db = new DbHelper(getApplicationContext());
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mhsList = db.list();
                String isian_nama = edNama.getText().toString();
                String isian_nim = edNim.getText().toString();
                String isian_nohp = edNoHp.getText().toString();
                if(isian_nama.isEmpty() || isian_nim.isEmpty() ||
                        isian_nohp.isEmpty()){
                    Toast.makeText(getApplicationContext(), "isian masih kosong", Toast.LENGTH_SHORT).show();
                }else{
                    // mhsList.add(new MhsModel(-1, isian_nama, isian_nim,isian_nohp));
                    boolean stts ;
                    if(!isEdit){
                        mm = new MhsModel(-1,isian_nama, isian_nim,
                                isian_nohp);
                        stts = db.simpan(mm);
                        edNama.setText("");
                        edNim.setText("");
                        edNoHp.setText("");
                    }else{
                        mm = new MhsModel(mm.getId(),isian_nama, isian_nim,
                                isian_nohp);
                        stts = db.ubah(mm);
                    }
                    if(stts){
                        Toast.makeText(getApplicationContext(), "Data Berhasil disimpan", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(), "gagal disimpan", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        Button btnLihat = (Button) findViewById(R.id.btnLihat);
        btnLihat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mhsList = db.list();

                if (mhsList.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Belum ada data", Toast.LENGTH_SHORT).show();

                } else {
                    Intent intent_list = new Intent(Terminal.this, ListMhsActivity.class);
                    intent_list.putParcelableArrayListExtra("mhsList", mhsList);
                    startActivity(intent_list);
                }
            }

        });
        btnKembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btnKembali = new Intent(Terminal.this, Home.class);
                startActivity(btnKembali);
            }
        });
    }
}
